﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroManager : MonoBehaviour
{
    [Header("Logic")]
    private Gyroscope gyro;
    private Quaternion rotation;
    private bool gyroActive;

    private static GyroManager instance;
    public static GyroManager Instance
    {
        get
        {
            if (instance ==  null)
            {
                instance = FindObjectOfType<GyroManager>();
                if (instance == null)
                {
                    instance = new GameObject("Spawned GyroManager", typeof(GyroManager)).GetComponent<GyroManager>();
                }
            }

            return instance;
        }
        set
        {
            instance = value;
        }
    }

    void Start()
    {
    }

    void Update()
    {
        if (gyroActive)
        {
            rotation = gyro.attitude;
        }
    }

    public void EnableGyroscope()
    {
        if (gyroActive)
        {
            return;
        }

        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;
            gyroActive = gyro.enabled;
        }
    }

    public Quaternion GetGyroRotation()
    {
        return rotation;
    }
}
