﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARTapToPlaceObject : MonoBehaviour
{
    public GameObject placementIndicator;
    public GameObject lookRight;
    public GameObject objectToPlace;
    public Button placeBuildingButton;
    public Button removeBuildingButton;

    private Pose placementPose;
    private ARRaycastManager arRaycastManager;
    private bool placementPoseIsValid;
    private bool isPlaced = false;
    private bool isActive = true;
    private GameObject instantiatedObj;
    private GameObject instantiatedLookRightObj;

    void Start()
    {
        arRaycastManager = FindObjectOfType<ARRaycastManager>();
        Button placeBuildingBtn = placeBuildingButton.GetComponent<Button>();
        placeBuildingBtn.onClick.AddListener(AddBuilding);
        Button removeBuildingBtn = removeBuildingButton.GetComponent<Button>();
        removeBuildingBtn.onClick.AddListener(RemoveBuilding);
        placeBuildingButton.gameObject.SetActive(true);
        removeBuildingButton.gameObject.SetActive(false);
    }

    void AddBuilding()
    {
        if (placementPoseIsValid && !isPlaced)
        {
            Vector3 offset = new Vector3(34.8f, -1.37f, -7.0f);

            isActive = false;
            instantiatedLookRightObj = Instantiate(lookRight, placementPose.position, placementPose.rotation);
            instantiatedObj = Instantiate(objectToPlace, placementPose.position + offset, placementPose.rotation);
            placeBuildingButton.gameObject.SetActive(false);
            removeBuildingButton.gameObject.SetActive(true);
            isPlaced = true;
        }
    }

    void RemoveBuilding()
    {
        if (isPlaced)
        {
            isPlaced = false;
            placeBuildingButton.gameObject.SetActive(true);
            removeBuildingButton.gameObject.SetActive(false);
            Destroy(instantiatedObj);
            Destroy(instantiatedLookRightObj);
            isActive = true;
        }
    }

    void Update()
    {
        UpdatePlacementPose();
        UpdatePlacementIndicator();
    }

    private void UpdatePlacementIndicator()
    {
        if (placementPoseIsValid && isActive)
        {
            placementIndicator.SetActive(true);
            placementIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
        }
        else
        {
            placementIndicator.SetActive(false);
        }
    }

    private void UpdatePlacementPose()
    {
        var screenCenter = Camera.current.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        var hits = new List<ARRaycastHit>();
        arRaycastManager.Raycast(screenCenter, hits, TrackableType.Planes);

        placementPoseIsValid = hits.Count > 0;
        if (placementPoseIsValid)
        {
            placementPose = hits[0].pose;

            var cameraForward = Camera.current.transform.forward;
            var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
            placementPose.rotation = new Quaternion(0, 0, 0, 0); // Quaternion.LookRotation(cameraBearing); Changed to stay stay still during phone rotation
        }
    }
}
