﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour
{
    public Transform cameraTransform;
    public CharacterController characterController;
    public Transform groundCheck;
    public Joystick joyMove;

    public float cameraSensitivity = 10f;
    public float moveSpeed = 0.1f;
    public float moveInputDeadZone = 8f;
    public float gravity = -9.81f;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    private int rightFingerId;
    private float cameraPitch;
    private float halfScreenWidth;
    private bool isGrounded;
    private Vector3 lookInput;
    private Vector3 moveInput;
    private Vector3 velocity;

    [Header("Tweaks")]
    [SerializeField] private Quaternion baseRotation = new Quaternion(0, 1, 0, 0);

    void Start()
    {
        halfScreenWidth = Screen.width / 2;
        GyroManager.Instance.EnableGyroscope();
    }

    void Update()
    {
        //Rotate with gyroscope
        //cameraTransform.localRotation = GyroManager.Instance.GetGyroRotation() * baseRotation;
        
        //Rotate with touch
        GetTouchInput();

        if (rightFingerId != -1)
        {
            LookAround();
        }

        //Move with joystick
        Move();
    }

    private void Move()
    {
        //Check if is on the ground and sets velocity
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        velocity.y += gravity * Time.deltaTime;

        Vector3 DirectionVector = new Vector3(joyMove.Horizontal, 0f, joyMove.Vertical);

        //Gravity
        characterController.Move(velocity * Time.deltaTime);
        //Move
        characterController.Move(transform.rotation * DirectionVector * moveSpeed);
    }

    private void LookAround()
    {
        cameraPitch = Mathf.Clamp(cameraPitch - lookInput.y, -90f, 90f);
        cameraTransform.localRotation = Quaternion.Euler(cameraPitch, 0, 0);

        transform.Rotate(transform.up, lookInput.x);
    }

    private void GetTouchInput()
    {
        for (int i = 0; i < Input.touchCount; i++)
        {
            Touch t = Input.GetTouch(i);

            switch (t.phase)
            {
                case TouchPhase.Began:
                    if (t.position.x > halfScreenWidth && rightFingerId == -1)
                    {
                        rightFingerId = t.fingerId;
                    }
                    break;
                case TouchPhase.Ended:
                case TouchPhase.Canceled:
                    if (t.fingerId == rightFingerId)
                    {
                        rightFingerId = -1;
                    }
                    break;
                case TouchPhase.Moved:
                    if (t.fingerId == rightFingerId)
                    {
                        lookInput = t.deltaPosition * cameraSensitivity * Time.deltaTime;
                    }
                    break;
                case TouchPhase.Stationary:
                    if (t.fingerId == rightFingerId)
                    {
                        lookInput = Vector2.zero;
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
